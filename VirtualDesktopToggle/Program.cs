﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsInput;
using WindowsInput.Native;

using System.Runtime.InteropServices;

namespace VirtualDesktopToggle {
 
    class Program {

        private static string ConfigName = "LastKeyCode";


        static void Main(string[] args) {              
            var inputSimulator = new InputSimulator();                  
            var modifiers = new HashSet<VirtualKeyCode>();
            var keycodeLeftRight = VirtualKeyCode.LEFT;


            if (GetLastKeyCode() == VirtualKeyCode.LEFT) {
                keycodeLeftRight = VirtualKeyCode.RIGHT;
            }
     
            modifiers.Add(VirtualKeyCode.LWIN);
            modifiers.Add(VirtualKeyCode.LCONTROL);
            inputSimulator.Keyboard.ModifiedKeyStroke(modifiers, keycodeLeftRight);
            SetLastKeyCode(keycodeLeftRight);

        }

        private static VirtualKeyCode GetLastKeyCode() {      
            var appSettings = ConfigurationManager.AppSettings;        
            var sepp = appSettings[ConfigName];
            String value = appSettings[ConfigName] ?? VirtualKeyCode.LEFT.ToString();              
            return (VirtualKeyCode)Enum.Parse(typeof(VirtualKeyCode), value);        
        }

        private static void SetLastKeyCode(VirtualKeyCode keycode) {

            try {
                var configFile = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                var settings = configFile.AppSettings.Settings;
                if (settings[ConfigName] == null) {
                    settings.Add(ConfigName, keycode.ToString());
                }
                else {
                    settings[ConfigName].Value = keycode.ToString();
                }
                configFile.Save(ConfigurationSaveMode.Modified);
                ConfigurationManager.RefreshSection(configFile.AppSettings.SectionInformation.Name);
            }
            catch (ConfigurationErrorsException) {
                Console.WriteLine("Error writing app settings");
            }
        }
    
    }
}
